all: tests

tests: test_main.o tests.o
	g++ -g -O0 -o $@ $^

%.o : %.cpp
	g++ -g -O0 -std=c++17 -o $@ -c $<

tests.o: test_main.cpp single_array.hpp factor_array.hpp list.hpp queue.hpp

insert_all:
	./tests --durations yes [insert]

exp:
	./tests --durations yes [exp]

clean:
	rm -f *.o tests
.PHONY: clean exp insert_all
