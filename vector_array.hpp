#include <cstddef>
#include <cstdlib>

template <typename T>
class vector_array
{
    public:
        vector_array() : m_values{nullptr}, m_size{0}, m_capacity{1} {}
        ~vector_array() {std::free(m_values);}

        std::size_t size() {return m_size;}

        T get(std::size_t index)
        {
            return m_values[index];
        }

        void push_back(T value)
        {
            if (size() == m_capacity || size() == 0)
            {
                auto ar = reinterpret_cast<T*>(std::malloc((m_capacity * 2) * sizeof(T)));
                for (std::size_t i = 0; i < size(); ++i)
                {
                    ar[i] = m_values[i];
                }

                ar[m_size] = value;
                std::free(m_values);
                m_values = ar;

                ++m_size;
                m_capacity *= 2;
            }
            else
            {
                m_values[m_size] = value;
                ++m_size;
            }
        }
 
        //void add(T item, std::size_t index) look in to the same method of SingleArray
        //there are nothing spectial to implement except workaround Factor
 
        //void remove(std::size_t index) look in to the same method of SingleArray
        //there are nothing spectial to implement except workaround Factor

    private:
        T *m_values;
        std::size_t m_size;
        std::size_t m_capacity;
};
