#include <cstddef>
#include <cstdlib>

template <typename T>
class single_array
{
    public:
        single_array() : m_values{nullptr}, m_size{0}, m_capacity{0} {}
        ~single_array() {std::free(m_values);}

        std::size_t size() {return m_size;}

        T get(std::size_t index)
        {
            return m_values[index];
        }

        void push_back(T value)
        {
            auto ar = allocate(m_size+1);
            for (std::size_t i = 0; i < m_size; ++i)
            {
                ar[i] = m_values[i];
            }

            ar[m_size] = value;
            std::free(m_values);
            m_values = ar;
            ++m_size;
        }

        void add(T item, std::size_t index)
        {
            auto ar = allocate(m_size+1);
            for (std::size_t i = 0; i < index; ++i)
            {
                ar[i] = m_values[i];
            }
            for (std::size_t i = m_size, j = m_size-1; i > index; --i)
            {
                ar[i] = m_values[j];
            }
            ar[index] = item;

            std::free(m_values);
            m_values = ar;
            ++m_size;
        }

        void remove(std::size_t index)
        {
            if (m_size == 0 || index >= m_size) return;
            
            for (std::size_t i = index+1; i < m_size; ++i)
            {
                m_values[i-1] = m_values[i];
            }

            --m_size;
            --m_capacity;
        }

    private:
        T *m_values;
        std::size_t m_size;
        std::size_t m_capacity;

        T *allocate(std::size_t s)
        {
            if (m_size < m_capacity)
            {
                return m_values;
            }
            else
            {
                auto array = reinterpret_cast<T*>(std::malloc((s) * sizeof(T)));
                ++m_capacity;
                return array;
            }
        }
};
