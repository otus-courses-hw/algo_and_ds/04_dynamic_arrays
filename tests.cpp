#include <catch2/catch.hpp>

#include "single_array.hpp"
#include "factor_array.hpp"
#include "vector_array.hpp"
#include "list.hpp"
#include "queue.hpp"

template <typename Array>
void putNtimes(Array &ar, std::size_t N)
{
    while(N != 0)
    {
        ar.push_back(666);
        --N;
    }
}

TEST_CASE("SingleArray add and remove", "[add][exp]")
{
    single_array<int> ar;
    putNtimes(ar, 3);

    ar.add(999, 1);
    CHECK(ar.size() == 4);
    for (auto i = 0; i < ar.size(); ++i)
    {
        if (i == 1)
            CHECK(ar.get(i) == 999);
        else
            CHECK(ar.get(i) == 666);
    }

    ar.remove(1);
    CHECK(ar.size() == 3);
    for (auto i = 0; i < ar.size(); ++i)
        CHECK(ar.get(i) == 666);

}

TEST_CASE("SingleArray N=10`000", "[insert]")
{
    single_array<int> ar;
    putNtimes(ar, 10'000);
    CHECK(ar.size() == 10'000);
}
TEST_CASE("FactorArray N=10'000 with factor 10", "[insert]")
{
    factor_array<int, 10> ar;
    putNtimes(ar, 10'000);
    CHECK(ar.size() == 10'000);
}
TEST_CASE("VectorArray N=10'000", "[insert]")
{
    vector_array<int> ar;
    putNtimes(ar, 10'000);
    CHECK(ar.size() == 10'000);
}
TEST_CASE("List N=10'000", "[insert]")
{
    list<int> ls;
    putNtimes(ls, 10'000);
    CHECK(ls.size() == 10'000);
}
TEST_CASE("Queue")
{
    queue<int, 2> q;
    q.enqueue(0, 1);
    q.enqueue(0, 2);
    q.enqueue(0, 3);
    CHECK(q.dequeue(0) == 1);
    q.enqueue(1, 666);
    q.enqueue(1, 999);
    CHECK(q.dequeue(0) == 2);
    CHECK(q.dequeue(1) == 666);
}

TEST_CASE("SingleArray N=100`000", "[insert]")
{
    single_array<int> ar;
    putNtimes(ar, 100'000);
    CHECK(ar.size() == 100'000);
}
TEST_CASE("FactorArray N=100'000 with factor 10", "[insert]")
{
    factor_array<int, 10> ar;
    putNtimes(ar, 100'000);
    CHECK(ar.size() == 100'000);
}
TEST_CASE("FactorArray N=100'000 with factor 1000", "[insert]")
{
    factor_array<int, 1000> ar;
    putNtimes(ar, 100'000);
    CHECK(ar.size() == 100'000);
}
TEST_CASE("VectorArray N=100'000", "[insert]")
{
    vector_array<int> ar;
    putNtimes(ar, 100'000); CHECK(ar.size() == 100'000);
}
TEST_CASE("List N=100'000", "[insert]")
{
    list<int> ls;
    putNtimes(ls, 100'000);
    CHECK(ls.size() == 100'000);
}
