#pragma once

#include <cstddef>
#include <cstdlib>

template <typename T>
class list
{
    public:
        list() : m_head{nullptr}, m_tail{nullptr}, m_size{0} {}
        ~list()
        {
            while (m_head != nullptr)
            {
                auto tmp = m_head->next;
                std::free(m_head);
                m_head = tmp;
            }
        }

        void push_back(T value)
        {
            auto n = new node{value, nullptr};
            if (m_head == nullptr)
            {
                m_head = n;
                m_tail = m_head;
            }
            else
            {
                m_tail->next = n;
                m_tail = n;
            }
            ++m_size;
        }

        T pop_front()
        {
            auto val = m_head->value;
            auto tmp = m_head->next;
            std::free(m_head);
            m_head = tmp;
            return val;
        }

        std::size_t size() {return m_size;}
        
    private:
        struct node
        {
            T value;
            node *next;
        };

        node *m_head;
        node *m_tail;
        std::size_t m_size;
};
