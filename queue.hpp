#include <cstddef>
#include "list.hpp"

template <typename T, std::size_t Prior>
class queue
{
    public:
        queue() = default;
        ~queue() = default;

        void enqueue(std::size_t p, T val)
        {
            values[p].push_back(val);
        }

        T dequeue(std::size_t p)
        {
            return values[p].pop_front();
        }

    private:
        list<T> values[Prior]; 
};
